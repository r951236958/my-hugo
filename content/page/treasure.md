---
title: 藏寶圖 - 寶箱位置
subtitle: 各地圖可獲得的靈體啟迪
comments: false
---

## 森林

![forest](img/treasure/forest.jpg)

冰霜: 4
神聖: 7
狂暴: 10

## 工廠

![factory](img/treasure/factory.jpg)

戰術: 4
閃電: 7
魂祭: 10

## 圖書館

![library](img/treasure/library.jpg)

靈誡: 4
防護: 7
火焰: 10

## 烏薩斯

![ursus](img/treasure/ursus.jpg)

狂暴/火焰/魂祭: 4
防護/閃電/神聖: 7
戰術/冰霜/靈誡: 10
