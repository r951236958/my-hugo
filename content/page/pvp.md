---
title: "競技場"
date: 2019-09-23T21:50:11+08:00
subtitle: "競技活動說明"
comments: false
tags: []
---

## 競技場段位、階級、獎勵

<div class="table-responsive-lg">
<table class="table table-dark custom-table" border="1" id="pvp_rank">
    <thead>
    <tr style="background-color:#212b31; color:#c1cfd8;">
        <th>段位</th>
        <th>階級</th>
        <th>晉升實戰值</th>
        <th>黑龍晶上限</th>
        <th>競技點獎勵</th>
        <th>賽季獎勵</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><img src="../img/pvp/rank_1.jpg" width="100px"/></td>
        <td>戰神隊長</td>
        <td>--</td>
        <td>1851</td>
        <td>565</td>
        <td><img src="../img/pvp/reward_1.png" width="100px"/></td>
    </tr>
    <tr>
        <td rowspan="3"><img src="../img/pvp/rank_2.jpg" width="100px"/></td>
        <td>高級勝利隊長</td>
        <td>--</td>
        <td>1605</td>
        <td>490</td>
        <td rowspan="3"><img src="../img/pvp/reward_2.png" width="100px"/></td>
    </tr>
    <tr>
        <td>中級勝利隊長</td>
        <td>220</td>
        <td>1481</td>
        <td>450</td>
    </tr>
    <tr>
        <td>初級勝利隊長</td>
        <td>205</td>
        <td>1358</td>
        <td>415</td>
    </tr>
    <tr>
        <td rowspan="3"><img src="../img/pvp/rank_3.jpg" width="100px"/></td>
        <td>高級堅毅隊長</td>
        <td>190</td>
        <td>1234</td>
        <td>375</td>
        <td rowspan="3"><img src="../img/pvp/reward_3.png" width="100px"/></td>
    </tr>
    <tr>
        <td>中級堅毅隊長</td>
        <td>175</td>
        <td>1173</td>
        <td>355</td>
    </tr>
    <tr>
        <td>初級堅毅隊長</td>
        <td>160</td>
        <td>1111</td>
        <td>340</td>
    </tr>
    <tr>
        <td rowspan="3"><img src="../img/pvp/rank_4.jpg" width="100px"/></td>
        <td>高級勇氣隊長</td>
        <td>145</td>
        <td>987</td>
        <td>300</td>
        <td rowspan="3"><img src="../img/pvp/reward_4.png" width="100px"/></td>
    </tr>
    <tr>
        <td>中級勇氣隊長</td>
        <td>130</td>
        <td>926</td>
        <td>280</td>
    </tr>
    <tr>
        <td>初級勇氣隊長</td>
        <td>100</td>
        <td>926</td>
        <td>265</td>
    </tr>
    </tbody>
</table>
</div>

- - -

### 競技場說明

<details>
  <summary>活動開放時間</summary>
    <ul>
        <li>週一至週日：12:00 ~ 12:00、20:00 ~ 21:00</li>
    </ul>
</details>

<details>
  <summary>匹配規則</summary>
    <ul>
        <li>系統匹配相同段位的玩家同步戰鬥，勝利可獲得賽季點數</li>
        <li>根據賽季點數晉升段位等級，段位越高，每局勝利可獲得黑龍晶獎勵越多</li>
        <li>每個 **<font color="#CD853F">賽段</font>** 結束的凌晨4:00點發放 **<font color="#CD853F">賽段</font>** 獎勵</li>
        <li>每4個 **<font color="#CD853F">賽段</font>** 為1個 **<font color="#DB7093">賽段</font>** ，第4賽段獎勵與賽季獎勵一起發放並重置段位，重置時所有段位降2級</li>
    </ul>
</details>

<details>
  <summary>晉升規則</summary>
    <ul>
        <li>勇氣隊長段位晉升賽：2局勝1局</li>
        <li>堅毅隊長段位晉升賽：3局勝2局</li>
        <li>勝利隊長段位晉升賽：5局勝3局</li>
        <li>每賽季最後一個賽段，高級勝利隊長晉升戰神隊長時，每天系統重置前取伺服器賽季點數持有最高的前100名玩家</li>
    </ul>
</details>

<details>
  <summary>競技獎勵</summary>
    <ul>
        <li>每局匹配對戰勝利/失敗均可獲得黑龍晶獎勵 (34/17)黑龍晶獎勵每賽段有獲取上限 (上限根據當前段位計算)</li>
        <li>初級勇氣隊長，每賽段黑龍晶獎勵上限864</li>
        <li>中級勇氣隊長，每賽段黑龍晶獎勵上限926</li>
        <li>高級勇氣隊長，每賽段黑龍晶獎勵上限987</li>
        <li>初級堅毅隊長，每賽段黑龍晶獎勵上限1111</li>
        <li>中級堅毅隊長，每賽段黑龍晶獎勵上限1173</li>
        <li>高級堅毅隊長，每賽段黑龍晶獎勵上限1234</li>
        <li>初級勝利隊長，每賽段黑龍晶獎勵上限1358</li>
        <li>中級勝利隊長，每賽段黑龍晶獎勵上限1481</li>
        <li>高級勝利隊長，每賽段黑龍晶獎勵上限1605</li>
        <li>戰神隊長，每賽段黑龍晶獎勵上限1851</li>
    </ul>

</details>

<details>
  <summary>競技BUFF</summary>
    <ul>
        <li>沈重鎧甲：肩負沈重的鎧甲，你專注於原本對付野外敵人的技能無法在蓋伊身上得到有效的發揮，自身造成的任何傷害降低32%，但是提升持有者264%的最大生命值。”蓋伊可跟野外那些敵人不大一樣”。持續至戰鬥結束。
        <li>神心之憐：你願意奉獻的精神得到了神的眷顧。最大生命值提升465%，同時提高340%自身造成的任何治療效果。不過神心有限，從第5個回合開始，提高的治療量將每6回合降低60%，直至100%。持續至戰鬥結束。</li>
        <li>專注之力：目標驅使著你不斷前行，你的機敏和專注吸納了競技場中防衛隊的祝福。提高持有者604%的最大生命值以及78%的出手速度。持續至戰鬥結束。</li>
        <li>點到為止：競技比賽旨在互相切磋，提高協作熟練，抵抗外界威脅。希望各位點到為止。降低持有者24%的猛攻概率，同時單體/群體技能控制效果的命中率會隨使用次數分別遞減11%和6%。持續至戰鬥結束。</li>
    </ul>
</details>
