---
title: 秘法塔謎團探險
subtitle: "無限期停更可前往巴哈或貼吧爬文"
date: 2019-09-19T11:22:23+08:00
tags: []
comments: false
---

## 討論串與貼文

- [微博關注@厄斯厄姆](https://www.weibo.com/u/2671164554?topnav=1&wvr=6&topsug=1&is_hot=1)
- [巴哈姆特 - 【密技】今日秘法塔謎團流程](https://forum.gamer.com.tw/C.php?page=1&bsn=36399&snA=146)
- [盖伊传说吧 - 每日秘法塔塔图攻略](https://tieba.baidu.com/p/6215972117)
- [答題題庫收集](https://docs.google.com/spreadsheets/d/1CB7Dd8zq7F-pI6i_l6xEYzo-jKGpBzWj7GQKvh8c9DQ/edit)

### 白老頭 - 答案

- 不然怎麼穿
- 全是是非題
- 屁
- 他沒去看露波秀
- 他負責蓋堡壘
- 會被偷走
- 倒著走
- 邁上另一隻腳
- 撿起來
- 一隻
- 蚊子
- 西瓜
- 麥爾斯
- 保持呼吸
- 天上
- 理髮師
- 傻瓜
- 隊長今天沒騎馬
- 不如床上舒服
- 他是水泥工
- 趴在桌上不好睡
