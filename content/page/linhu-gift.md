---
title: "送禮特殊劇情 - 林虎"
date: 2019-09-23T21:30:36+08:00
subtitle: "是不是神又怎樣？"
tags: ["gyee", "linhu"]
---

## 選項與好感度

| 選項 | 好感度 |
| :--: | :--: |
| 讚嘆林虎的博學 | 未知 |
| 幫忙引薦 | 未知 |
| 表示神與戰鬥力無關 | 900 |

## 劇情截圖

{{< gallery >}}
{{< figure link="/img/linhu/gift/1.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/2.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/3.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/4.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/5.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/6.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/7.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/8.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/9.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/10.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/11.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/12.jpg" caption="表示神與戰鬥力無關+900" >}}
{{< figure link="/img/linhu/gift/13.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/14.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/15.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/16.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/17.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/18.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/19.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/20.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/21.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/22.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/23.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/24.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/25.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/26.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/27.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/28.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/29.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/30.jpg" caption="" >}}
{{< figure link="/img/linhu/gift/31.jpg" caption="" >}}
{{< /gallery >}}
