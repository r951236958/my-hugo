---
title: "送禮特殊劇情 - 林虎 2"
date: 2019-10-01T00:31:44+08:00
subtitle: "林虎餓很久了"
tags: []
---

## 選項與好感度

| 選項 | 好感度 |
| :--: | :--: |
| 星之森 | 未知 |
| 採購供品 | 900 |
| 紅港科技交流會 | 未知 |

## 劇情截圖

{{< gallery >}}
{{< figure link="/img/linhu/gift2/1.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/2.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/3.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/4.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/5.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/6.jpg" caption="採購供品+900" >}}
{{< figure link="/img/linhu/gift2/7.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/8.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/9.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/10.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/11.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/12.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/13.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/14.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/15.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/16.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/17.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/18.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/19.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/20.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/21.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/22.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/23.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/24.jpg" caption="" >}}
{{< figure link="/img/linhu/gift2/25.jpg" caption="" >}}
{{< /gallery >}}
