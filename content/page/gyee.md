---
title: 參考資訊 part II
date: 2019-09-23T13:50:19.000+00:00
subtitle: 一些常會需要查詢的資料
comments: false
tags: []

---
## 開眼等級

| 開眼等級 | 武器提升 | 開眼碎片 | 星粹 |
| :---: | :---: | :---: | :---: |
| 1 | -- | -- | -- |
| 2 | 7% | 40 | -- |
| 3 | 12% | 80 | -- |
| 4 | 17% | 160 | -- |
| 5 | 27% | 240 | 160 |
| 6 | 42% | 320 | 240 |

## 武器符文合成

| 武器符文 | 碎片合成數量 |
| :---: | :---: |
| LV.1 三角I | 1 |
| LV.2 四角I | 2 |
| LV.3 三角II | 4 |
| LV.4 四角II | 6 |
| LV.5 五角II | 6 |
| LV.6 三角III | 8 |
| LV.7 四角III | 13 |
| LV.8 五角III | 22 |
| LV.9 六角III | 33 |
| LV.10 三角VI | 35 |

## 公社建設獎勵

建設任務次數：**70 /週**；建設任務與寶箱**每週一凌晨4:00重新設置**。

    ⁃	建設任務獎勵
    ⁃	15: 金幣30000
    ⁃	35: 蓋伊萬能啟迪25
    ⁃	70: 萬能印象碎片5

| 任務次數 | 寶箱獎勵 |
| --- | --- |
| 15 | 金幣30000 |
| 35 | 蓋伊萬能啟迪25 |
| 70 | 萬能印象碎片5 |

## 藏寶圖

<table border="1" id="treasure">
<tr>
<th>藏寶圖位置</th>
<th>隨機獲得</th>
<th>必獲得</th>
</tr>
<tr>
<td rowspan="3" style="text-align: center;"><h4>森林</h4>%3C%20foldergallery%20src%3D%22imgs/treasure/forest%22%20%3E</td>
<td>(秘法家)冰霜啟迪x4</td>
<td rowspan="3">靈體萬能啟迪x7</td>
</tr>
<tr>
<td>(驅魔使)神聖啟迪x7</td>
</tr>
<tr>
<td>(劍術師)狂暴x10</td>
</tr>
<tr>
<td rowspan="3" style="text-align: center;"><h4>工廠</h4>%3C%20foldergallery%20src%3D%22imgs/treasure/factory%22%20%3E</td>
<td>(秘法家)閃電x7</td>
<td rowspan="3">靈體萬能啟迪x7</td>
</tr>
<tr>
<td>(驅魔使)魂祭x10</td>
</tr>
<tr>
<td>(劍術師)戰術x4</td>
</tr>
<tr>
<td rowspan="3" style="text-align: center;"><h4>圖書館</h4>%3C%20foldergallery%20src%3D%22imgs/treasure/library%22%20%3E</td>
<td>(秘法家)火焰x10</td>
<td rowspan="3">靈體萬能啟迪x7</td>
</tr>
<tr>
<td>(驅魔使)靈誡x4</td>
</tr>
<tr>
<td>(劍術師)防護x7</td>
</tr>
</table>