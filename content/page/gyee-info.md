---
title: 參考資訊
date: 2019-09-23T13:50:19.000+00:00
subtitle: 一些常會需要查詢的資料
comments: false
tags: []

---
## 蓋伊開眼

### 提升開眼等級

每個蓋伊初始的開眼等級皆為1級，使用蓋伊印象（萬能印象）可提升開眼等級，開眼時會優先消耗**蓋伊印象**，蓋伊印像不足時將使用萬能印象補充。

* 開眼等級1：40
* 開眼等級2：80
* 開眼等級3：160
* 開眼等級4：240
* 開眼等級5：320

### 覺醒

蓋伊開眼等級達到最高時，通過消耗**星粹**覺醒，可將開眼等級提升1級（最高上限6）

* 開眼等級4：160星粹
* 開眼等級5：240星粹

## 蓋伊武器喚靈與靈體魂器充能

### 對照圖示

| 蓋伊武器喚靈 | 靈體命輪魂器充能 |
| --- | --- |
| {{< foldergallery src="imgs/attributes/spiritcaller" >}} | {{< foldergallery src="imgs/attributes/horcrux" >}} |

### 喚靈對應魂器位置擁有屬性表

<style> .table-3 table tr td {text-align：center;   
} </ style>

<div class =“ ox-hugo-table table-3”> <div> <！-<div class =“ table-caption”> <span class =“ table-number”> Table-3 </ span>：下面表格放置中對齊</ div>-> </ div>

| 蓋伊喚靈 | 魂器位置 | 隨機屬性 |
| --- | --- | --- |
| <img src="../img/attributes/h.png?classes=shadow" width="30"> 火靈 | 壹 | 主屬性</br>活力</br>耐受 |
| <img src="../img/attributes/j.png?classes=shadow" width="30"> 金靈 | 貳 | 猛攻</br>猛傷</br>抵抗</br>活力 |
| <img src="../img/attributes/d.png?classes=shadow" width="30"> 地靈 | 三 | 猛攻</br>命中</br>精準</br>格擋 |
| <img src="../img/attributes/s.png?classes=shadow" width="30"> 森靈 | 肆 | 精通</br>物穿</br>精準</br>格擋 |
| <img src="../img/attributes/sh.png?classes=shadow" width="30"> 水靈 | 伍 | 精通</br>法穿</br>耐受</br>增效 |
| <img src="../img/attributes/m.png?classes=shadow" width="30"> 秘靈 | 陸 | 主屬性</br>命中</br>抵抗 |

</ div>

## 武器符文合成

| 武器符文 | 合成所需數量 |
| --- | --- |
| LV.1 三角I | 1 |
| LV.2 四角I | 2 |
| LV.3 三角II | 4 |
| LV.4 四角II | 6 |
| LV.5 五角II | 6 |
| LV.6 三角III | 8 |
| LV.7 四角III | 13 |
| LV.8 五角III | 22 |
| LV.9 六角III | 33 |
| LV.10 三角VI | 35 |

## 藏寶圖

<table border="1" id="treasure">
<tr>
<th>藏寶圖位置</th>
<th>隨機獲得</th>
<th>必獲得</th>
</tr>
<tr>
<td rowspan="3" style="text-align: center;"><h4>森林</h4>{{< foldergallery src="imgs/treasure/forest" >}}</td>
<td>(秘法家)冰霜啟迪x4</td>
<td rowspan="3">靈體萬能啟迪x7</td>
</tr>
<tr>
<td>(驅魔使)神聖啟迪x7</td>
</tr>
<tr>
<td>(劍術師)狂暴x10</td>
</tr>
<tr>
<td rowspan="3" style="text-align: center;"><h4>工廠</h4>{{< foldergallery src="imgs/treasure/factory" >}}</td>
<td>(秘法家)閃電x7</td>
<td rowspan="3">靈體萬能啟迪x7</td>
</tr>
<tr>
<td>(驅魔使)魂祭x10</td>
</tr>
<tr>
<td>(劍術師)戰術x4</td>
</tr>
<tr>
<td rowspan="3" style="text-align: center;"><h4>圖書館</h4>{{< foldergallery src="imgs/treasure/library" >}}</td>
<td>(秘法家)火焰x10</td>
<td rowspan="3">靈體萬能啟迪x7</td>
</tr>
<tr>
<td>(驅魔使)靈誡x4</td>
</tr>
<tr>
<td>(劍術師)防護x7</td>
</tr>
</table>