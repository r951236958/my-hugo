---
title: "在Finder中無法找到檔案"
date: 2019-11-17T10:59:31+08:00
categories:
- uncategorised
tags:
- notag
draft: true
---

## MacOS 在Finder中找不到檔案

這幾天無意間發現剛剛另存的檔案、下載的檔案，從Finder要開啟時看不到那個檔案在哪裡，上網找了一下找到幾篇文章，後來是參考 [這篇文章](https://macorel.blogspot.com/2019/05/finder-wont-update.html?showComment=1573956413818) 所提供的方式來解決的，目前看起來已經正常，後續再持續觀察看看...

### 解決方法

開啟Finder視窗，<kbd> ⌘ </kbd> + <kbd> ⇧ </kbd> 輸入 `~/Library/Preferences/com.apple.finder.plist` 即可前往這文件所在的資料夾，只需要將他移至垃圾桶，並清空垃圾桶，再重新啟動Finder就可以了，不過需要注意的是，這個文件就是Finder偏好設定的設定文件，刪除此檔案會使 **還原Finder設定** 這點請自行斟酌。

後續再補充關於如何重啟Finder的方法，或是也是參考原文中的方式。
