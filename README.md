---

Example [Hugo] website using GitLab with [Netlify](https://www.netlify.com/).

---

[![Netlify Status](https://api.netlify.com/api/v1/badges/2505caaa-5a49-4a6c-a335-faf77728ab00/deploy-status)](https://app.netlify.com/sites/suspicious-jennings-7e4ffa/deploys)

## Netlify Configuration

In order to build this site with Netlify, simply log in or register at 
https://app.netlify.com/, then select "New site from Git" from the top
right. Select GitLab, authenticate if needed, and then select this
project from the list. Netlify will handle the rest.

In the meantime, you can take advantage of all the great GitLab features
like merge requests, issue tracking, epics, and everything else GitLab has
to offer.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[hosting-on-netlify]: https://gohugo.io/hosting-and-deployment/hosting-on-netlify/

---

### Netlify設定說明

在設定文件 `netlify.toml` 中關於 `hugo` 指令後加入 `--cleanDestinationDir` `--forceSyncStatic` `--gc` `--ignoreCache` `--minify` 等額外參數，這些參數的作用如下：

```
$ hubo [options]
--cleanDestinationDir #構建前先清理目標文件夾，即 public
--forceSyncStatic #強制同步 static 文件夾
--gc #構建後執行一些清理任務（刪除掉一些沒用的緩存文件）
--ignoreCache #構建時忽略緩存
--minify #壓縮網頁（Debug 時慎用）
```

參考來源: [原文連結](https://www.sulinehk.com/post/deploying-hugo-website-to-netlify/)

### 使用Netlify託管HUGO網站如何設置主題

- [官方文檔](https://s0gohugo0io.icopy.site/hosting-and-deployment/hosting-on-netlify/#use-hugo-themes-with-netlify)
- [git-submodule指令](https://www.andrewhoog.com/post/git-submodule-for-hugo-themes/)
- [Git - Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
